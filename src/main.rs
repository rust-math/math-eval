use rustyline::Editor;
use math_eval::Eval;

fn main() {
    let mut rl = Editor::<()>::new();
    let mut eval = Eval::new();

    while let Ok(line) = rl.readline(">>> ") {
        match eval.eval(&line) {
            Ok(value) => println!("-> {:.10}", value),
            Err(err) => eprintln!("-> error: {}", err)
        }

        rl.add_history_entry(line);
    }
}
