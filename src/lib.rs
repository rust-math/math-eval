#[macro_use]
extern crate failure;

use std::collections::HashMap;
use math_parser::{
    parser::{Error as ParseError, Node, Types},
    tokenizer::Operation,
    types::*
};
use math_types::BigInt;
use num_traits::*;

type Fraction = math_types::Fraction<BigInt>;

/// An error that occured during evaluation
#[derive(Debug, Fail)]
pub enum Error {
    #[fail(display = "integer parse error: {}", _0)]
    IntegerParseError(String),
    #[fail(display = "integer overflow during operation: {:?} {:?} {:?}", _0, _1, _2)]
    OpIntegerOverflow(Fraction, Operation, Fraction),
    #[fail(display = "parse error: {}", _0)]
    ParseError(#[cause] ParseError),
    #[fail(display = "no such variable: {}", _0)]
    UnknownVar(String)
}

/// The evaluator state, with defined variables for example
#[derive(Clone, Default)]
pub struct Eval {
    pub vars: HashMap<String, Node>
}
impl Eval {
    /// Create a new evaluator
    pub fn new() -> Self {
        Self::default()
    }

    /// Parse and evaluate some code
    pub fn eval(&mut self, code: &str) -> Result<Fraction, Error> {
        let ast = math_parser::parse(code).to_result().map_err(Error::ParseError)?;
        self.eval_node(ast.node().borrowed())
    }
    /// Evaluate an AST node
    pub fn eval_node(&'_ mut self, node: Node<rowan::RefRoot<'_, Types>>) -> Result<Fraction, Error> {
        fn float(frac: Fraction) -> f64 {
            (frac * Fraction::from(BigInt::new(1_000_000_000_000u64))).floor().as_i128() as f64 / 1_000_000_000_000.0
        }
        fn frac(mut float: f64) -> Fraction {
            float *= 1_000_000_000_000.0;
            Fraction::new(
                BigInt::from(float.min(std::i128::MAX as f64).max(std::i128::MIN as f64) as i128),
                BigInt::from(1_000_000_000_000u64)
            )
        }
        if let Some(root) = Root::cast(node) {
            self.eval_node(root.value())
        } else if let Some(paren) = Paren::cast(node) {
            self.eval_node(paren.value())
        } else if let Some(value) = Value::cast(node) {
            value.as_bigfraction().ok_or_else(|| {
                let ident = value.node().leaf_text();
                Error::IntegerParseError(ident.map(|s| s.as_str().into()).unwrap_or_default())
            })
        } else if let Some(ident) = Ident::cast(node) {
            let ident = ident.as_str();
            match self.vars.get(ident).cloned() {
                Some(node) => self.eval_node(node.borrowed()),
                None => {
                    match ident {
                        "pi" => Ok(Fraction::PI()),
                        _ => {
                            let mut product = Fraction::one();
                            for c in ident.chars() {
                                let value = self.vars.get(&c.to_string()).cloned()
                                    .ok_or_else(|| Error::UnknownVar(ident.to_string()))?;
                                let value = self.eval_node(value.borrowed())?;
                                product = product.clone().checked_mul(value.clone())
                                    .ok_or_else(|| Error::OpIntegerOverflow(product, Operation::Mul, value))?;
                            }
                            Ok(product)
                        }
                    }
                }
            }
        } else if let Some(op) = OperationNode::cast(node) {
            let operation = op.operation();

            if operation == Operation::Equal {
                let (var, value) = match (Ident::cast(op.left()), Ident::cast(op.right())) {
                    (Some(var), _) => (var, op.right()),
                    (_, Some(var)) => (var, op.left()),
                    (None, None) => return Err(Error::UnknownVar(op.node().to_string()))
                };
                // Make the variable itself undefined, so that evaluation below
                // will fail if it's an infinite loop
                self.vars.remove(var.as_str());
                let result = self.eval_node(value)?;
                self.vars.insert(var.as_str().into(), value.owned());
                return Ok(Fraction::from(result));
            }

            let left = self.eval_node(op.left())?;
            let right = self.eval_node(op.right())?;

            let left_clone = left.clone();
            let right_clone = right.clone();
            let overflow = |result: Option<Fraction>| {
                result.ok_or_else(|| Error::OpIntegerOverflow(left_clone, operation, right_clone))
            };

            match operation {
                Operation::Equal => unreachable!(),
                Operation::Add => overflow(left.checked_add(right)),
                Operation::Sub => overflow(left.checked_sub(right)),
                Operation::Mul => overflow(left.checked_mul(right)),
                Operation::Div => overflow(left.checked_div(right)),
                Operation::Rem => overflow(left.checked_rem(right)),
                Operation::Pow => Ok(frac(float(left).powf(float(right)))),
                Operation::BitAnd => Ok(Fraction::from(BigInt::from(left.floor().into_unsigned() & right.floor().unsigned()))),
                Operation::BitOr => Ok(Fraction::from(BigInt::from(left.floor().into_unsigned() | right.floor().unsigned()))),
                Operation::BitXor => Ok(Fraction::from(BigInt::from(left.floor().into_unsigned() ^ right.floor().unsigned()))),
                Operation::BitShiftL => Ok(Fraction::from(BigInt::from(left.floor().into_unsigned() << right.floor().unsigned().as_u128() as usize))),
                Operation::BitShiftR => Ok(Fraction::from(BigInt::from(left.floor().into_unsigned() >> right.floor().unsigned().as_u128() as usize))),
            }
        } else if let Some(op) = Unary::cast(node) {
            let operation = op.operation();
            let value = self.eval_node(op.value())?;
            match operation {
                //UnaryOperation::BitNot => Ok(Fraction::from(!value.floor())),
                UnaryOperation::BitNot => Ok(-value - BigInt::one()),
                UnaryOperation::Negate => Ok(-value)
            }
        } else if let Some(fac) = Factorial::cast(node) {
            let n = float(self.eval_node(fac.value())?);
            let approx = math_algos::gamma::facf(n);
            Ok(frac(approx))
        } else {
            unimplemented!("Unknown type of node")
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::cell::RefCell;

    thread_local! {
        static EVAL: RefCell<Eval> = RefCell::new(Eval::new());
    }

    fn test<T: Into<Fraction>>(code: &str, result: T) {
        EVAL.with(|eval| {
            assert_eq!(eval.borrow_mut().eval(code).expect("error").simplify(), result.into());
        })
    }

    #[test]
    fn basic() {
        test("1 + 2 * 3 / 4", Fraction::new(BigInt::new(5), BigInt::new(2)));
        test("11 % 3", BigInt::new(2));
    }
    #[test]
    fn power() {
        test("2 ** 3.3", Fraction::new(BigInt::new(9849155306759i64), BigInt::new(1000000000000i64)));
    }
    #[test]
    fn variable() {
        test("x = 3", BigInt::new(3));
        test("x = 2 + 3", BigInt::new(5));
        test("x + x", BigInt::new(10));
    }
    #[should_panic]
    #[test]
    fn variable_loop() {
        test("x = 3", BigInt::new(3));
        test("x = x", BigInt::new(0));
    }
    #[test]
    fn bitwise() {
        test("0b101 | 0b011", BigInt::new(0b111));
        test("0b101 & 0b011", BigInt::new(0b001));
        test("0b101 ^ 0b011", BigInt::new(0b110));
        test("0b11 << 3", BigInt::new(0b11000));
        test("0b11 >> 1", BigInt::new(0b1));
    }
    #[test]
    fn unary() {
        test("-1", BigInt::new(-1));
        test("-(5 + 2)", BigInt::new(-7));
        test("~0b001", BigInt::new(-2) /* 111...110 = -2 because of how the sign bit works */);
    }
    #[test]
    fn factorial() {
        test("0.1!", Fraction::new(BigInt::new(095135076i64), BigInt::new(100000000i64)));
        test("4!", BigInt::new(24));
        test("4.1!", Fraction::new(BigInt::new(2793175373i64), BigInt::new(100000000i64)));
    }
}
